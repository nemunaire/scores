\header {
  title = "Jazz Beats"
  tagline = ##f
}
\paper {
  ragged-last-bottom = ##f
}
\version "2.22.0"

#(define mydrums '(
                    ( ridecymbal    cross    #f   5)
                    ( ridecymbala   xcircle  #f   5)
                    ( crashcymbal   cross    #f   6)
                    ( splashcymbal  harmonic #f   6)
                    ( pedalhihat    cross    #f  -5)
                    ( hihat         cross    #f   5)
                    ( snare         default  #f   1)
                    ( sidestick     cross    #f   1)
                    ( lowmidtom     default  #f   0)
                    ( lowtom        default  #f  -1)
                    ( hightom       default  #f   3)
                    ( bassdrum      default  #f  -3)))

% Base
\score {
  \new DrumStaff
  <<
    \numericTimeSignature
    \time 4/4
    \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums)

    \new DrumVoice {
      \stemUp
      \drummode {
        \mark "Swing feel!"
        \repeat unfold 2 {
          cymr4 cymr8 cymr
        }
      }
    }
    \new DrumVoice {
      \stemDown
      \drummode {
        \repeat unfold 2 {
          r4 hhp
        }
      }
    }
  >>

  \layout {
  }
}

% Patterns
\score {
  \new DrumStaff
  <<
    \numericTimeSignature
    \time 4/4
    \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums)

    \new DrumVoice {
      \stemUp
      \drummode {
        \mark "Pattern 1"
        \repeat unfold 4 {
          cymr4 cymr8 cymr
        }

        \break
        \mark "Pattern 2"
        \repeat unfold 4 {
          cymr4 cymr8 cymr
        }

        \break
        \mark "Pattern 3"
        \repeat unfold 4 {
          cymr4 cymr8 cymr
        }

        \break
        \mark "Pattern 4"
        \repeat unfold 4 {
          cymr4 cymr8 cymr
        }
      }
    }
    \new DrumVoice {
      \stemDown
      \drummode {
        bd4 << sn hhp >>
        \repeat unfold 3 {
          r4 << sn hhp >>
        }

        bd8 sn hhp4
        r4 hhp4
        r8 sn hhp4
        r4 hhp4

        bd4 hhp8 sn
        r4 hhp8 sn
        r4 hhp8 sn
        r4 hhp8 sn

        bd8 sn hhp sn
        r8 sn hhp sn
        r8 sn hhp sn
        r8 sn hhp sn
      }
    }
  >>

  \layout {
  }
}

% Interlude
\score {
  \new DrumStaff
  <<
    \numericTimeSignature
    \time 4/4
    \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums)

    \new DrumVoice {
      \stemUp
      \drummode {
        \mark "Interlude"
        \repeat unfold 8 {
          cymr4 cymr8 cymr
        }
      }
    }
    \new DrumVoice {
      \stemDown
      \drummode {
        bd8 sn hhp4
        sn4 hhp8 (sn sn1)
        r8 sn hhp4
        sn4 hhp8 (sn sn1)
      }
    }
  >>

  \layout {
  }
}

% The full monty
\score {
  \new DrumStaff
  <<
    \numericTimeSignature
    \time 4/4
    \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums)

    \new DrumVoice {
      \stemUp
      \drummode {
        \mark "The full monty"
        \repeat unfold 4 {
          cymr4 cymr8 cymr
        }

        \repeat unfold 4 {
          cymr4 cymr8 cymr
        }

        \repeat unfold 4 {
          cymr4 cymr8 cymr
        }

        \repeat unfold 4 {
          cymr4 cymr8 cymr
        }
      }
    }
    \new DrumVoice {
      \stemDown
      \drummode {
        bd4 << sn hhp >>
        \repeat unfold 3 {
          r4 << sn hhp >>
        }

        bd8 sn hhp4
        r4 hhp4
        r8 sn hhp4
        r4 hhp4

        bd4 hhp8 sn
        r4 hhp8 sn
        r4 hhp8 sn
        r4 hhp8 sn

        bd8 sn hhp sn
        r8 sn hhp sn
        r8 sn hhp sn
        r8 sn hhp sn
      }
    }
  >>

  \layout {
  }
}
