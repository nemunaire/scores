\header {
  title = "I Love Rock 'n Roll"
  composer = "Joan Jett & The Blackhearts"
  tagline = ##f
}

#(define mydrums '(
                    ( ridecymbal    cross    #f   5)	
                    ( ridecymbala   xcircle  #f   5)
                    ( crashcymbal   cross    #f   6)	
                    ( splashcymbal  harmonic #f   6)
                    ( pedalhihat    cross    #f  -5)	
                    ( hihat         cross    #f   5)
                    ( snare         default  #f   1)	
                    ( sidestick     cross    #f   1)
                    ( lowmidtom     default  #f   0)	
                    ( lowtom        default  #f  -1)
                    ( hightom       default  #f   3)	
                    ( bassdrum      default  #f  -3)))

drh = \drummode {
        % Intro
        s s r r
        cymc8 hh8 hh4 hh8 hh hh4
        \repeat percent 2 {hh8 hh hh4 hh8 hh hh4}
        \time 3/4
        hh8 hh hh4 hh8 hh
        \time 4/4
        \repeat percent 2 {hh8 hh hh4 hh8 hh hh4}

        % Verse
        \repeat volta 2 {
          \repeat percent 4 {hh8 hh hh4 hh8 hh hh4}

          % Pre-chorus
          hh8 hh hh4 hh8 hh hh4
          hh8 hh hh4 cymc8 hh hh4
          \repeat percent 3 {hh8 hh hh4 hh8 hh hh4}
          hh8 hh hh4 hh8 hh hh hh
          \time 2/4
          cymc4 s

          % Chorus
          \time 4/4
          cymc8 hh hh4 hh8 hh hh4
          \repeat percent 2 {hh8 hh hh4 hh8 hh hh4}
          \time 3/4
          hh8 hh hh4 hh8 hh
          \time 4/4
          \repeat percent 2 {hh8 hh hh4 hh8 hh hh4}
        }

        % Guitar solo
        \repeat percent 4 {hh8 hh hh4 hh8 hh hh4}

        % Pre-chorus 3
        hh8 hh hh4 hh8 hh hh4
        hh8 hh hh4 cymc8 hh hh4
        \repeat percent 3 {hh8 hh hh4 hh8 hh hh4}
        hh8 hh hh4 hh hh
        \time 2/4 hh8 cymc8 hh4
        \time 4/4

        % Chorus 3
        \repeat percent 3 {s hh s hh}
        s hh s s

        % Chorus 3a
        cymc8 hh hh4 hh8 hh hh4
        \repeat percent 2 {hh8 hh hh4 hh8 hh hh4}
        \time 3/4
        hh8 hh hh4 hh8 hh
        \time 4/4

        % Chorus 3b
        \repeat percent 3 {hh8 hh hh4 hh8 hh hh4}
        \time 3/4
        hh8 hh hh4 hh8 hh
        \time 4/4

        % End
        hh8 hh cymc4 hh8 hh hh4
        \repeat percent 2 {hh8 hh hh4 hh8 hh hh4}
        \time 3/4
        hh8 hh hh4 hh8 hh
        \time 4/4

        % End A
        \repeat percent 3 {hh8 hh hh4 hh8 hh hh4}
        hh8 hh hh4 hh8 hh cymc4
      }
drl = \drummode {
        \mark Intro
        r4 r4 \f sn16 sn16 sn16 sn16 sn16 sn16 sn16 sn16
        bd8 bd sn4 bd8 bd sn4
        \repeat percent 2 {bd8 bd sn4 bd8 bd sn4}
        \time 3/4
        bd8 bd sn4 bd8 bd
        \time 4/4
        \repeat percent 2 {bd8 bd sn4 bd8 bd sn4}

        \mark Verse
        \repeat volta 2 {
          \repeat percent 4 {bd8 bd sn4 bd8 bd sn4} 

          \mark Pre-chorus
          bd8 bd sn4 bd8 bd sn4
          bd8 bd sn4 bd8 bd sn4
          \repeat percent 3 {bd8 bd sn4 bd8 bd sn4}
          bd8 bd sn4 << bd8 sn8 >> bd << bd8 sn8 >> bd
          \time 2/4
          << bd8 sn8 >> sn sn16 sn sn8

          \mark Chorus
          \time 4/4
          bd8 bd sn4 bd8 bd sn4
          \repeat percent 2 {bd8 bd sn4 bd8 bd sn4}
          \time 3/4
          bd8 bd sn4 bd8 bd
          \time 4/4
          \repeat percent 2 {bd8 bd sn4 bd8 bd sn4}
        }

        \mark "Guitar solo"
        \repeat percent 4 {bd8 bd sn4 bd8 bd sn4}

        \mark "Pre-chorus 3"
        bd8 bd sn4 bd8 bd sn4
        bd8 bd sn4 bd8 bd sn4
        \repeat percent 3 {bd8 bd sn4 bd8 bd sn4}
        bd8 bd sn4 bd8 bd sn4
        \time 2/4
        bd8 bd sn4

        \mark "Chorus 3"
        \time 4/4
        \repeat percent 3 {bd8 bd sn4 bd8 bd sn4}
        bd8 bd sn4 bd8 bd sn16 sn sn8

        \mark "Chorus 3a"
        bd8 bd sn4 bd8 bd sn4
        \repeat percent 2 {bd8 bd sn4 bd8 bd sn4}
        \time 3/4
        bd8 bd sn4 bd8 bd
        \time 4/4

        \mark "Chorus 3b"
        \repeat percent 3 {bd8 bd sn4 bd8 bd sn4}
        \time 3/4
        bd8 bd sn4 bd8 bd
        \time 4/4

        \mark "End"
        bd8 bd sn4 bd8 bd sn4
        \repeat percent 2 {bd8 bd sn4 bd8 bd sn4}
        \time 3/4
        bd8 bd sn4 bd8 bd
        \time 4/4

        \mark "End A"
        \repeat percent 3 {bd8 bd sn4 bd8 bd sn4}
        bd8 bd sn4 bd8 bd bd4
      }

\score {
  \new DrumStaff
  <<
    \tempo 4 = 100
    \numericTimeSignature
    \time 4/4
    \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums)
    
    \new DrumVoice { \stemUp \drh }
    \new DrumVoice { \stemDown \drl }
  >>

  \layout {
  }
  \midi { \tempo 4 = 100 }
}