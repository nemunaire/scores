\header {
  title = "Série D"
  tagline = ##f
}
\paper {
  ragged-last-bottom = ##f
}
\version "2.22.0"

#(define mydrums '(
                    ( ridecymbal    cross    #f   5)
                    ( ridecymbala   xcircle  #f   5)
                    ( crashcymbal   cross    #f   6)
                    ( splashcymbal  harmonic #f   6)
                    ( pedalhihat    cross    #f  -5)
                    ( hihat         cross    #f   5)
                    ( snare         default  #f   1)
                    ( sidestick     cross    #f   1)
                    ( lowmidtom     default  #f   0)
                    ( lowtom        default  #f  -1)
                    ( hightom       default  #f   3)
                    ( bassdrum      default  #f  -3)))

% Intro
\score {
  \new DrumStaff
  <<
    \numericTimeSignature
    \time 4/4
    \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums)

    \drummode {
      \repeat volta 3 {
        toml4->^"D" \tuplet 6/2 {sn8^"G" sn^"G" sn^"D" sn^"D" sn^"G" sn^"G"}
        tomh4->^"D" \tuplet 6/2 {sn8^"G" sn^"G" sn^"D" sn^"D" sn^"G" sn^"G"}
        \mark "×3"
      }

      \tuplet 6/2 {toml8^"D" sn8^"G" sn^"D" sn^"D" sn^"G" sn^"G"}
      \tuplet 6/2 {sn8^"D" sn^"D" sn^"G" sn^"G" sn^"D" sn^"D"}
      \tuplet 6/2 {tomh8^"G" tomh^"G" tomh^"D" tomh^"D" tomh^"G" tomh^"G"}
      \tuplet 6/2 {toml8^"D" toml^"D" toml^"G" toml^"G" toml^"D" toml^"D"}
    }
  >>

  \layout {
  }
}

% First part
\score {
  \new DrumStaff
  <<
    \numericTimeSignature
    \time 4/4
    \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums)

    \drummode {
      \repeat volta 2 {
        toml8.->^"D" sn32^"G" sn^"G" sn8.->^"D" sn32^"G" sn^"G"
        sn32^"D" sn^"D" sn^"G" sn^"G" sn^"D" sn^"D" sn^"G" sn^"G"
        tomh8.->^"D" sn32^"G" sn^"G"
      }

      toml8.->^"D" sn32^"G" sn^"G" sn8.^"D" sn32^"G" sn^"G"
      sn32^"D" sn^"D" sn^"G" sn^"G" sn^"D" sn^"D" sn^"G" sn^"G"
      tomh16->^"D" sn32^"G" sn^"G" sn^"D" sn^"D" sn^"G" sn^"G"
      \repeat unfold 2 {
        toml16->^"D" sn32^"G" sn^"G" sn^"D" sn^"D" sn^"G" sn^"G"
        tomh16->^"D" sn32^"G" sn^"G" sn^"D" sn^"D" sn^"G" sn^"G"
      }
    }
  >>

  \layout {
  }
}

% Second part
\score {
  \new DrumStaff
  <<
    \numericTimeSignature
    \time 4/4
    \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums)

    \drummode {
      \repeat volta 2 {
        toml8.->^"D" sn32^"G" sn^"G" tomh8.->^"D" sn32^"G" sn^"G"
        sn32^"D" sn^"D" tomh16->^"G" sn32^"D" sn^"D" sn^"G" sn^"G"
        tomh16->^"D" sn32^"G" sn^"G" sn32^"D" sn^"D" tomh16->^"G"

        \once \override Score.RehearsalMark.break-visibility =
          #end-of-line-visible
        \once \override Score.RehearsalMark.self-alignment-X =
          #RIGHT
        \mark "×3"
      }

      toml16->^"D"
      \repeat unfold 2 {
        sn32^"G" sn^"G" sn^"D" sn^"D" tomh16->^"G"
        sn32^"D" sn^"D" sn^"G" sn^"G" tomh16->^"D"
      }
      sn32^"G" sn^"G" sn^"D" sn^"D" tomh16->^"G"
    }
  >>

  \layout {
  }
}

% Third part
\score {
  \new DrumStaff
  <<
    \numericTimeSignature
    \time 4/4
    \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums)

    \overrideProperty Score.NonMusicalPaperColumn.line-break-system-details
    #'((alignment-distances . (10)))

    \drummode {
      \repeat volta 2 {
        toml8.->^"D" sn32^"G" sn^"G" sn8.->^"D" sn32^"G" sn^"G"
        sn^"D" sn^"D" tomh->^"G" sn^"D" sn^"D" sn^"G" sn^"G"
        tomh->^"D" sn^"G" sn^"G" sn^"D" sn^"G" sn8:64^"G"
        \mark "×3"
      }

      toml32->^"D"
      \repeat unfold 2 {
        sn32^"G" sn^"G" sn^"D" sn^"D" tomh->^"G"
        sn32^"D" sn^"D" sn^"G" sn^"G" tomh->^"D"
      }
      sn32^"G" sn^"G" sn^"D" sn^"D" tomh->^"G" sn8.:64^"D"
    }
  >>

  \layout {
  }
}

% Fourth part
\score {
  \new DrumStaff
  <<
    \numericTimeSignature
    \time 4/4
    \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums)

    \drummode {
      \repeat volta 2 {
        toml8.->^"D" sn32^"G" sn^"G"
        sn16->^"D" sn32^"G" sn^"G" sn32^"D" sn^"D"
        tomh16->^"G" sn32^"D" sn^"D" sn32^"G" sn^"G" tomh->^"D"
        sn32^"G" sn^"G" sn^"D" sn^"D" tomh->^"G"
        sn32^"D" sn^"D" sn^"G" sn^"G" tomh->^"D"
        sn32^"G"

        \once \override Score.RehearsalMark.break-visibility =
          #end-of-line-visible
        \once \override Score.RehearsalMark.self-alignment-X =
          #RIGHT
        \mark "×3"
      }

      toml32->^"D" sn32^"G" sn^"G" sn^"D" sn^"D" tomh->^"G" sn32^"D" sn^"D"
      sn^"G" sn^"G" tomh16->^"D" sn32^"G" sn^"G" sn^"D" sn^"D"
      tomh->^"G" sn32^"D" sn^"D" sn^"G" sn^"G" tomh16->^"D" sn32^"G"
      sn^"G" sn^"D" sn^"D" tomh->^"G" sn32^"D" sn^"D" sn^"G" sn^"G"
    }
  >>

  \layout {
  }
}