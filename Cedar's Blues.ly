\header {
  title = "Cedar's Blues"
  composer = "Cedar Walton's Quintet"
  tagline = ##f
}
\version "2.22.0"
\include "swing.ly"

#(ly:set-option 'midi-extension "mid")

#(define mydrums '(
                    ( ridecymbal    cross    #f   5)
                    ( ridecymbala   xcircle  #f   5)
                    ( crashcymbal   cross    #f   6)
                    ( splashcymbal  harmonic #f   6)
                    ( pedalhihat    cross    #f  -5)
                    ( hihat         cross    #f   5)
                    ( snare         default  #f   1)
                    ( sidestick     cross    #f   1)
                    ( lowmidtom     default  #f   0)
                    ( lowtom        default  #f  -1)
                    ( hightom       default  #f   3)
                    ( bassdrum      default  #f  -3)))

bbar = {
  bes,8^\markup{B\flat 7} bes,8
  f8 f8
  aes8 aes8
  bes8 bes8
}
ebar = {
  ees,8^\markup{E\flat 7} ees,8
  bes,8 bes,8
  des8 des8
  ees8 ees8
}
gbar = {
  g,8^\markup{G7} g,8
  d8 d8
  f8 f8
  g8 g8
}
cbar = {
  c8^\markup{C-} c8
  g8 g8
  bes8 bes8
  c'8 c'8
}
fbar = {
  f,8^\markup{F7} f,8
  c8 c8
  ees8 ees8
  f8 f8
}

\score {
  \header {
    piece = "(Melody in \"2\" feel)"
  }
  <<
    \new Staff \with {midiInstrument = #"alto sax" instrumentName = "Sax"} {
      \numericTimeSignature
      \clef treble
      \key bes \major
      \time 4/4
      \repeat volta 2 {
        \tuplet 3/2 { bes'8 c'' bes' } a' bes' c'' bes' a' bes'
        d''4 f''8 c'' r d'' r f'~
        f'4 g'8 f' bes'4 c''8 bes'
        d''4 bes'8 aes' r << c'-> dis' f' aes' des'' >> r << dis'->~ e'~ g'~ bes'~ ees''~ >>

        << dis'1~ e'~ g'~ bes'~ ees''~ >>
        << dis'2 e' g' bes' ees'' >> r8 << aes'-> ees' c' a gis >> r << bes'->~ f' d' b ais >>
        << bes'1~ f' d' b ais >>
        << bes' f' d' b ais >>

        << f'2 a' c'' >> << e'8 gis' b' >> << f' a' c'' >> r << e'~ gis' b' >>
        << e'2 gis' b' >> << ees'8 g' bes' >> << e' gis' b' >> r << ees'~ g' bes' >>
        << ees'2 g' bes' >> << a'8 fis' d' >> << bes' g' ees' >> r << a'~ fis' d' >>
        << a'1 fis' d' >>

        << des'2 f' aes' >> << g'8 e' c' >> << des' f' aes' >> r << g'~ e' c' >>
        << g'2 e' c' >> << fis'8 dis' b >> << g' e' c' >> r << fis'->~ dis' b >>
        << fis'2 dis' b >> << f'8 d' bes >> << fis' dis' b >> r << f'~ d' bes >>
        << f'4 d' bes >> << d'~ a dis >> << d'2 a dis >>
      }
    }

    \new Staff \with {midiInstrument = #"fretless bass" instrumentName = "Bass"} <<
      \repeat unfold 4 {
        s1 \noBreak s1 \noBreak
        s1 \noBreak s1 \break
      }
      {
        \numericTimeSignature
        \clef bass
        \key bes \major
        \time 4/4
        \repeat volta 2 {
          \bbar \bbar \bbar
          bes,8 bes,8
          aes,8 aes,8
          des8^\markup{D\flat 7\flat 9} des
          ees^\markup{E\flat 7\flat 9} ees

          \ebar
          ees,8 ees,8
          bes,8 bes,8
          aes,8^\markup{A\flat 7\flat 9} aes,
          aes,8^\markup{A\flat 7\flat 9} aes,
          \bbar \bbar

          \repeat unfold 4 { r4 f r f }

          \repeat unfold 3 { r4 ees r ees }
          r4 ees~ ees2
        }
      }
    >>

    \new DrumStaff \with {instrumentName = "Drums"} {
      \tempo 4 = 120
      \numericTimeSignature
      \time 4/4
      \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums)
      \repeat volta 2 <<
        \new DrumVoice {
          \stemUp \drummode {
            \repeat unfold 7 {
              cymr4 \tripletFeel 8 {cymr8 cymr}
            }
            cymr8 cymc8 cymr8 << cymc8 cymr8 >>

            \repeat unfold 3 {
              cymr4 \tripletFeel 8 {cymr8 cymr}
            }
            cymr8 cymc8 cymr8 << cymc8 cymr8 >>
            \repeat unfold 4 {
              cymr4 \tripletFeel 8 {cymr8 cymr}
            }

            \repeat unfold 15 {
              cymr4 <<
                \tripletFeel 8 {cymr8 cymr}
                cymc8
              >>
            }
            cymr4 \tripletFeel 8 {cymr8 cymr}
          }
        }
        \new DrumVoice {
          \stemDown \drummode {
            \repeat unfold 32 {
              r4 hhp4
            }
          }
        }
      >>
    }
  >>

  \layout { }
  \midi { }
}

\score {
  \header {
    piece = "Solo"
  }
  <<
    \new Staff \with {midiInstrument = #"alto sax" instrumentName = "Sax"} {
      \numericTimeSignature
      \repeat unfold 3 {
        s1 \noBreak s1 \noBreak
        s1 \noBreak s1 \break
      }
    }

    \new Staff \with {midiInstrument = #"fretless bass" instrumentName = "Bass"} {
      \tempo 4 = 120
      \numericTimeSignature
      \clef bass
      \key bes \major
      \time 4/4
      \repeat volta 2 {
        \bbar \ebar \bbar \bbar

        \ebar \ebar \bbar \gbar

        \cbar \fbar \bbar
        c8^\markup{C-} c8 bes8 bes8
        f,8^\markup{F7} f,8 ees8 ees8
      }
    }

    \new DrumStaff \with {instrumentName = "Drums"} {
      \tempo 4 = 120
      \numericTimeSignature
      \time 4/4
      \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums)
      \repeat volta 2 <<
        \new DrumVoice {
          \stemUp \drummode {
            \repeat unfold 22 {
              cymr4 \tripletFeel 8 {cymr8 cymr}
            }
          }
        }
        \new DrumVoice {
          \stemDown \drummode {
            \repeat unfold 5 {
              bd4 hhp r hhp
              r hhp r hhp
            }
            bd4 hhp r hhp
            bd4 sn8 sn r sn sn4
          }
        }
      >>
    }
  >>

  \layout { }
  \midi { }
}